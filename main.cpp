// systemctl restart ds18b20.service
// g++ -Wall main.cpp OneWire.cpp -o ds18b20 -lpaho-mqttpp3 -lpaho-mqtt3a -lwiringPi
// ./ds18b20 tcp://localhost:1883 DS18B20 5000
// ./ds18b20 DFLT_SERVER_ADDRESS DFLT_CLIENT_ID DFLT_DELAY_MILLISEC


#include "OneWire.h"
#include <wiringPi.h>

#include <sstream>

#include <iostream>
#include <cstdlib>
#include <string>
#include <thread>	// For sleep
#include <atomic>
#include <chrono>
#include <cstring>

#include "mqtt/async_client.h"

using namespace std;

const string DFLT_SERVER_ADDRESS	{ "tcp://localhost:1883" };
const string DFLT_CLIENT_ID		{ "async_ds18b20" };
const int DFLT_DELAY_MILLISEC = 1000;
const auto TIMEOUT = std::chrono::seconds( 10);

static int N = 2; 	// Number of sensors

const string TOPICS[] = { "ANSWERPRO/DS18B20/1/TEMP", "ANSWERPRO/DS18B20/2/TEMP" };

const int  QOS = 1;



int main(int argc, char* argv[])
{
//    int arg1 = atoi(argv[1]);
//    cout << x << endl;

	OneWire * ds18b20;
	uint64_t roms[N];
	try {
        ds18b20 = new OneWire(25);
		ds18b20->oneWireInit();

		ds18b20->searchRom(roms, N);
		cout << "---------------------------------" << endl;
		cout << "devices = " << N << endl;
		cout << "---------------------------------" << endl;
		for (int i = 0; i < N; i++) {
			cout << "addr T[" << (i + 1) << "] = " << roms[i] << endl;
		}
		cout << "---------------------------------" << endl;
	} catch (exception & e) {
		cerr << "ERROR: wiringpi" << e.what() << endl;
		return 1;
	}

	try {
        string  address  = (argc > 1) ? string(argv[1]) : DFLT_SERVER_ADDRESS,
                clientID = (argc > 2) ? string(argv[2]) : DFLT_CLIENT_ID;
        int delayMillisec = (argc > 3) ? atoi(argv[3]) : DFLT_DELAY_MILLISEC;



        cout << "Initializing for server '" << address << "'..." << endl;
        mqtt::async_client client(address, clientID);
        mqtt::connect_options conopts;
        cout << "  ...OK" << endl;
		cout << "\nConnecting..." << endl;
		mqtt::token_ptr conntok;

		try {
            conntok = client.connect(conopts);
            cout << "Waiting for the connection..." << endl;
            conntok->wait();
            cout << "  ...OK" << endl;
        } catch (exception & e) {
            cerr << "ERROR: MQTT connection " << e.what() << endl;
            return 1;
        }

		while(1) {
			for (int i = 0; i < N; i++) {
				double temperature;
				temperature = OneWire::getTemp(ds18b20, roms[i]);
				cout << "Temperature[" << i << "]=" << temperature << endl;
				ostringstream strs;
				strs << temperature;
				string str_temperature = strs.str();
				char dest[50] = "";
				strcat(dest, str_temperature.c_str());
				cout << "Sending message" << " TOPIC:" << TOPICS[i] << ":" << dest << endl;
				mqtt::message_ptr pubmsg = mqtt::make_message(TOPICS[i], dest);
				pubmsg->set_qos(QOS);
				try {
                    client.publish(pubmsg)->wait_for(TIMEOUT);
				}
				catch (const exception& ex) {
				    cout << "Error: " << ex.what() << endl;
				}


				cout << "  ...OK" << endl;
			}
            cout << "Waiting..." << delayMillisec << " milliseconds" << endl;
			std::this_thread::sleep_for(std::chrono::milliseconds(delayMillisec));
        }

		// Disconnect
		cout << "\nDisconnecting..." << endl;
		conntok = client.disconnect();
		conntok->wait();
		cout << "  ...OK" << endl;
	}
	catch (const mqtt::exception& exc) {
		cerr << exc.what() << endl;
		return 1;
	}

 	return 0;
}

